<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider {

	/**
	 * Register any other marcos for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
		parent::boot();

		require base_path() . '/resources/macros/base.php';
	}

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
