<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	//protected $fillable = ['account', 'email', 'password', 'name'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['account', 'password', 'remember_token'];

	/**
	 * The attributes included from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $visible = ['first_name', 'last_name'];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'status' => 'boolean',
		'is_system' => 'boolean',
		//'options' => 'array',
	];

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

//	public function getIsSystemAttribute()
//	{
//		return $this->attributes['is_system'] == 1;
//	}
//
//	protected $appends = ['is_system'];

	/**
	 * Query Scopes
	 *
	 * @return mixed
	 */
	public function scopeValid($query)
	{
		return $query->where('status', '=', 1);
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		User::observe(new \App\Observers\UserObserver());
	}

}
