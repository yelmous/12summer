<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tags';

	/**
	 * 關聯文章
	 *
	 * @var array
	 */
	public function article()
	{
		return $this->belongsTo('\App\Models\Article');
	}

    /**
	 * 依關鍵字取得關聯文章
	 *
	 * @var array
	 */
	public function articles($id)
	{
		return \App\Models\Article::Valid()->whereIn('id', \App\Models\Tag::where('id', '=', $id)->lists('article_id'))->get();
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		Tag::observe(new \App\Observers\NormalObserver());
	}

}
