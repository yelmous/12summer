<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Key extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'key';

	/**
	 * 搜尋
	 *
	 * @var array
	 */
	public function scopeKey($query, $key)
	{
		return $query->where('func_id', '=', $key);
	}

}
