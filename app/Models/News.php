<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'news';

	protected $dates = ['deleted_at'];

	/**
	 * 搜尋
	 *
	 * @var array
	 */
	public function scopeSearch($query, $search)
	{
		$words = explode(" ", $search);

		foreach ($words as $word) {
			$query = $query->where(function($query) use ($word)
			{
				$query->where('title', 'like', '%'.$word.'%');
				$query->orwhere('content', 'like', '%'.$word.'%');
			});
		}

		return $query;
	}

	/**
	 * Query Scopes
	 *
	 * @return mixed
	 */
	public function scopeValid($query)
	{
		return $query->where('status', '=', 1);
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		News::observe(new \App\Observers\NormalObserver());
	}

}
