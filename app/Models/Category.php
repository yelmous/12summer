<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	protected $dates = ['deleted_at'];

    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['deleted_at'];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'is_system'     => 'boolean',
		'is_sponsor'    => 'boolean',
		'status'        => 'boolean',
	];

	/**
	 * 搜尋
	 *
	 * @var array
	 */
	public function scopeSearch($query, $search)
	{
		$words = explode(" ", $search);

		foreach ($words as $word) {
			$query = $query->where(function($query) use ($word)
			{
				$query->where('category_name', 'like', '%'.$word.'%');
			});
		}

		return $query;
	}

	/**
	 * Query Scopes
	 *
	 * @return mixed
	 */
	public function scopeValid($query)
	{
		return $query->where('status', '=', 1);
	}

	/**
	 * 關聯文章
	 *
	 * @var array
	 */
	public function articles()
	{
		return $this->hasMany('\App\Models\Article', 'category_id', 'id');
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		Category::observe(new \App\Observers\NormalObserver());
	}

}
