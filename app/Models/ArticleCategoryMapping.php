<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleCategoryMapping extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'articlecatemappings';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['special_id', 'category_id', 'article_id', 'cate_index'];

    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at'];

	/**
	 * 頻道
	 *
	 * @var array
	 */
	public function special()
	{
		return $this->belongsTo('App\Models\Special');
	}

	/**
	 * 關聯小分類
	 *
	 * @var array
	 */
	public function cate()
	{
		return $this->belongsTo('App\Models\SpectialCategory', 'category_id', 'id');
	}

	/**
	 * 關聯文章
	 *
	 * @var array
	 */
	public function article()
	{
		return $this->belongsTo('App\Models\Article');
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		ArticleCategoryMapping::observe(new \App\Observers\NormalObserver());
	}

}
