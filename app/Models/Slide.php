<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'slides';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['deleted_at'];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'status'        => 'boolean',
		'is_new_window' => 'boolean',
		'is_text'       => 'boolean',
		'is_main'       => 'boolean',
	];

	/**
	 * 搜尋
	 *
	 * @var array
	 */
	public function scopeSearch($query, $search)
	{
		$words = explode(" ", $search);

		foreach ($words as $word) {
			$query = $query->where(function($query) use ($word)
			{
				$query->where('title', 'like', '%'.$word.'%');
				$query->orwhere('sub_title', 'like', '%'.$word.'%');
				$query->orwhere('content_1', 'like', '%'.$word.'%');
				$query->orwhere('content_2', 'like', '%'.$word.'%');
				$query->orwhere('content_3', 'like', '%'.$word.'%');
				$query->orwhere('slide_img', 'like', '%'.$word.'%');
				$query->orwhere('slide_url', 'like', '%'.$word.'%');
			});
		}

		return $query;
	}

	/**
	 * Query Scopes
	 *
	 * @return mixed
	 */
	public function scopeValid($query)
	{
		return $query->where('status', '=', 1);
	}

	/**
	 * 依代碼查詢
	 *
	 * @return mixed
	 */
	public function scopeCode($query, $code)
	{
		return $query->where('status', '=', 1)->where('special_code', '=', $code)->first();
	}

	/**
	 * 關聯對應(頻道)
	 *
	 * @var array
	 */
	public function mappings()
	{
		return $this->hasMany('\App\Models\SlideMapping');
	}

    /**
	 * 依鍵值取得關聯頻道
	 *
	 * @var array
	 */
	public function specials($id)
	{
		return DB::table('slidemappings_view')->where('slide_id', '=', $id)->groupBy('special_id')->get();
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		Slide::observe(new \App\Observers\NormalObserver());
	}

}
