<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'articles';

	protected $dates = ['deleted_at'];

    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['deleted_at', 'category'];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'is_promote'    => 'boolean',
		'is_sponsor'    => 'boolean',
		'is_draft'      => 'boolean',
		'is_check'      => 'boolean',
		'status'        => 'boolean',
	];

    /**
	 * The attributes appended to the model's JSON form.
	 *
	 * @var array
	 */
    protected $appends = ['category_name', 'cate_mapping'];

    /**
	 * @category_name
	 */
    public function getCategoryNameAttribute()
    {
        return $this->category->category_name;
    }

    /**
	 * @category_name
	 */
    public function getCateMappingAttribute()
    {
        return $this->cate_mappings;
    }

	/**
	 * 搜尋
	 *
	 * @var array
	 */
	public function scopeSearch($query, $search)
	{
		$words = explode(" ", $search);

		foreach ($words as $word) {
			$query = $query->where(function($query) use ($word)
			{
				$query->where('title', 'like', '%'.$word.'%');
				$query->orwhere('sub_title', 'like', '%'.$word.'%');
				$query->orwhere('preview', 'like', '%'.$word.'%');
				$query->orwhere('content', 'like', '%'.$word.'%');
			});
		}

		return $query;
	}

	/**
	 * Query Scopes
	 *
	 * @return mixed
	 */
	public function scopeValid($query)
	{
		return $query->where('status', '=', 1);
	}

	/**
	 * 依頻道取得文章
	 *
	 * @return mixed
	 */
	public function scopeSpecial($query, $id)
	{
		return $query
            ->whereHas('mappings', function($q) use ($id)
            {
                $q->where('special_id', '=', $id);

            });
        /*
        return $query
            ->whereExists(function($query) use ($id)
            {
                $query->select(\DB::raw(1))
                      ->from('articlemappings')
                      ->whereRaw('319_articlemappings.special_id = '.$id.' and 319_articlemappings.article_id = 319_articles.id');
            });
        */
	}

	/**
	 * 依頻道分類取得文章
	 *
	 * @return mixed
	 */
	public function scopeSpecialCate($query, $id, $cate)
	{
		return $query
            ->whereHas('cate_mappings', function($q) use ($id, $cate)
            {
                $q->where('special_id', '=', $id)->where('category_id', '=', $cate);

            });
	}

	/**
	 * 促銷文章
	 *
	 * @return mixed
	 */
	public function scopeWide($query)
	{
		return $query->where('is_promote', '=', 1);
	}

	/**
	 * 關聯對應(頻道)
	 *
	 * @var array
	 */
	public function mappings()
	{
		return $this->hasMany('\App\Models\ArticleMapping');
	}

	/**
	 * 關聯對應(頻道分類)
	 *
	 * @var array
	 */
	public function cate_mappings()
	{
		return $this->hasMany('\App\Models\ArticleCategoryMapping');
	}

	/**
	 * 關聯分類
	 *
	 * @var array
	 */
	public function category()
	{
		return $this->belongsTo('\App\Models\Category');
	}

	/**
	 * 關聯編輯推薦BOX
	 *
	 * @var array
	 */
	public function boxes()
	{
		return $this->hasMany('\App\Models\Box');
	}

	/**
	 * 關聯區域
	 *
	 * @var array
	 */
	public function zipCode()
	{
		return $this->belongsTo('App\Models\Zip', 'zip', 'zip');
	}

	/**
	 * 關聯縣市
	 *
	 * @var array
	 */
	public function cityCode()
	{
		return \App\Models\Zip::Valid()->where('city_no', '=', $this->city_no)->first();
	}

	/**
	 * 關聯鄉鎮市區(多筆)
	 *
	 * @return mixed
	 */
	public function arealist()
	{
        return \App\Models\Zip::Valid()->whereIn('area_no', explode(',', $this->area_no_list))->orderByRaw('area_no')->get();
	}

	/**
	 * 相似文章(三則)
	 *
	 * @return mixed
	 */
	public function similars($special_id)
	{
        $special = \App\Models\Special::find($special_id);
        return \App\Models\Article::Valid()->whereIn('id', $special->articles()->where('article_id', '!=', $this->id)->lists('article_id'))->orderByRaw('RAND()')->take(3)->get();
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		Article::observe(new \App\Observers\NormalObserver());
	}

}
