<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecialCategory extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'specialcategories';

	protected $dates = ['deleted_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	/**
	 * 依代碼查詢
	 *
	 * @return mixed
	 */
	public function scopeCode($query, $id, $code)
	{
		return $query->where('status', '=', 1)->where('special_id', '=', $id)->where('cate_code', '=', $code);
	}

	/**
	 * 關聯頻道
	 *
	 * @var array
	 */
	public function special()
	{
		return $this->belongsTo('\App\Models\Special');
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		SpecialCategory::observe(new \App\Observers\NormalObserver());
	}

}
