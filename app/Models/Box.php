<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Box extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'boxes';

	protected $dates = ['deleted_at'];

    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['deleted_at'];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'status'        => 'boolean',
	];

	/**
	 * 搜尋
	 *
	 * @var array
	 */
	public function scopeSearch($query, $search)
	{
		$words = explode(" ", $search);

		foreach ($words as $word) {
			$query = $query->where(function($query) use ($word)
			{
				$query->where('title', 'like', '%'.$word.'%');
				$query->orwhere('content', 'like', '%'.$word.'%');
			});
		}

		return $query;
	}

	/**
	 * 關聯文章
	 *
	 * @var array
	 */
	public function article()
	{
		return $this->belongsTo('\App\Models\Article');
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		Box::observe(new \App\Observers\NormalObserver());
	}

}
