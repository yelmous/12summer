<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'setting';

    public $timestamps = false;

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'status' => 'boolean',
	];

    /**
	 * 有效
	 *
	 * @var array
	 */
	public function scopeValid($query)
	{
		return $query->where('status', '=', 1);
	}

    /**
	 * 依鍵值取得設定物件
	 *
	 * @var array
	 */
	public function scopeSet($query, $type, $key)
	{
        return $query->where('status', '=', 1)->where('type', '=', $type)->where('param_id', '=', $key)->first();
	}

}
