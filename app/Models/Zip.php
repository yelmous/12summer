<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zip extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'zip';

	protected $dates = ['deleted_at'];

    /**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'city_code' => 'string',
		'city_no'   => 'string',
		'area_no'   => 'string',
		'zip'       => 'string',
		'status'    => 'boolean',
	];

	/**
	 * Query Scopes
	 *
	 * @return mixed
	 */
	public function scopeValid($query)
	{
		return $query->where('status', '=', 1);
	}

	/**
	 * 依代碼取得鄉鎮市區
	 *
	 * @return mixed
	 */
	public function scopeArea($query, $id)
	{
		return $query->where('area_no', '=', $id);
	}

	/**
	 * 依郵遞區號取得關聯文章
	 *
	 * @var array
	 */
	public function articles($zip)
	{
        return \App\Models\Article::Valid()->where('zip', '=', $zip)->get();
	}

}
