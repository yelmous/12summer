<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleMapping extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'articlemappings';

	/**
	 * 頻道
	 *
	 * @var array
	 */
	public function special()
	{
		return $this->belongsTo('\App\Models\Special');
	}

	/**
	 * 關聯文章
	 *
	 * @var array
	 */
	public function article()
	{
		return $this->belongsTo('\App\Models\Article');
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		ArticleMapping::observe(new \App\Observers\NormalObserver());
	}

}
