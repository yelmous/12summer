<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Special extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'specials';

	protected $dates = ['deleted_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['deleted_at'];

	/**
	 * 搜尋
	 *
	 * @var array
	 */
	public function scopeSearch($query, $search)
	{
		$words = explode(" ", $search);

		foreach ($words as $word) {
			$query = $query->where(function($query) use ($word)
			{
				$query->where('title', 'like', '%'.$word.'%');
				$query->orwhere('special_code', 'like', '%'.$word.'%');
				$query->orwhere('special_name', 'like', '%'.$word.'%');
				$query->orwhere('overture', 'like', '%'.$word.'%');
				$query->orwhere('memo', 'like', '%'.$word.'%');
			});
		}

		return $query;
	}

	/**
	 * Query Scopes
	 *
	 * @return mixed
	 */
	public function scopeValid($query)
	{
        $query = $query->where('status', '=', 1)
            ->where('start_date', '<=', \Carbon\Carbon::now())
            ->where('end_date', '>=', \Carbon\Carbon::now());
		return $query;
	}

	/**
	 * 依代碼查詢
	 *
	 * @return mixed
	 */
	public function scopeCode($query, $code)
	{
		return $query->where('status', '=', 1)->where('special_code', '=', $code)->first();
	}

	/**
	 * 關聯分類
	 *
	 * @var array
	 */
	public function categories()
	{
		return $this->hasMany('\App\Models\SpecialCategory');
	}

	/**
	 * 關聯對應(文章)
	 *
	 * @var array
	 */
	public function article_mappings()
	{
		return $this->hasMany('\App\Models\ArticleMapping');
	}

    /**
	 * 依鍵值取得關聯文章
	 *
	 * @var array
	 */
	public function articles()
	{
		return \DB::table('articlemappings_view')->where('special_id', '=', $this->id);
	}

    /**
	 * 依鍵值取得關聯分類文章
	 *
	 * @var array
	 */
	public function catearticles($cate)
	{
		return \DB::table('articlecatemappings_view')->where('special_id', '=', $this->id)->where('cate_id', '=', $cate);
	}

	/**
	 * 關聯對應(幻燈片)
	 *
	 * @var array
	 */
	public function slide_mappings()
	{
        return $this->hasMany('\App\Models\SlideMapping');
	}

    /**
	 * 依鍵值取得關聯幻燈片
	 *
	 * @var array
	 */
	public function slides()
	{
        return \DB::table('slides')->whereIn('id', $this->hasMany('\App\Models\SlideMapping')->lists('slide_id'))->orderBy('order_by')->get();
		//return \DB::table('slidemappings_view')->where('special_id', '=', $this->id)->groupBy('slide_id')->get();
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		Special::observe(new \App\Observers\NormalObserver());
	}

}
