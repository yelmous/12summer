<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TableLog extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'logs';

	/**
	 * 搜尋
	 *
	 * @var array
	 */
	public function scopeSearch($query, $table_name, $table_key)
	{
		return $query->where('table_name', $table_name)->where('table_key', $table_key)->orderBy('created_at', 'asc')->get();
	}

	/**
	 * 關聯使用者
	 *
	 * @var array
	 */
	public function user()
	{
		return $this->belongsTo('\App\User');
	}

}
