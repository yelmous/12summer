<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SlideMapping extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'slidemappings';

	/**
	 * 頻道
	 *
	 * @var array
	 */
	public function special()
	{
		return $this->belongsTo('App\Models\Special');
	}

	/**
	 * 關聯幻燈片
	 *
	 * @var array
	 */
	public function slide()
	{
		return $this->belongsTo('\App\Models\Slide');
	}

	/**
	 * boot
	 *
	 * @var array
	 */
	public static function boot()
	{
		parent::boot();
		ArticleMapping::observe(new \App\Observers\NormalObserver());
	}

}
