<?php namespace App\Http\Controllers;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * 官網首頁
	 *
	 * @return Response
	 */
	public function index()
	{
        if (\Input::has( config('site.cache_flash') )) {
            \Cache::flush();
            var_dump('Cache flush.');
        }

        $special = \App\Models\Special::Valid()->orderBy('order_by')->first();
        if ($special) {
            return redirect('/'.$special->special_code);
        } else {
            return abort(404);
        }
	}

	/**
	 * 頻道頁
	 *
	 * @return Response
	 */
	public function special($id)
	{
        //\DB::connection()->enableQueryLog();

        $special = \Cache::remember('special_'.$id, config('site.cache_expires'), function() use ($id)
        {
            return \App\Models\Special::Code($id);
        });

        // 文章
        // 寬版(促銷)
        $wide = \Cache::remember('special_wide_'.$id, config('site.cache_expires'), function() use ($special)
        {
            return \App\Models\Article::Valid()->Special($special->id)->Wide()->orderByRaw('RAND()')->take(3)->get();
        });
        // 窄版(所有文章扣除寬版(促銷))
        $narrow = \Cache::remember('special_narrow_'.$id, config('site.cache_expires'), function() use ($special, $wide)
        {
            return \App\Models\Article::Valid()->Special($special->id)->whereNotIn('id', $wide->lists('id'))->orderByRaw('RAND()')/*->take(15)*/->get();
        });
        $article = \Cache::remember('special_article_'.$id, config('site.cache_expires'), function() use ($special, $wide)
        {
            return \App\Models\Article::Valid()->Special($special->id)->where('id', '<>', $wide[0]->id)->orderByRaw('RAND()')->take(5)->get();
        });

        //$queries = \DB::getQueryLog();
        //var_dump($queries);

		return view('pages.special')
            ->with('special', $special)
            ->with('wide', $wide)
            ->with('narrow', $narrow)
            ->with('article', $article);
	}

	/**
	 * 頻道分類頁
	 *
	 * @return Response
	 */
	public function category($code, $cate)
	{
        $special = \Cache::remember('special_'.$code, config('site.cache_expires'), function() use ($code)
        {
            return \App\Models\Special::Code($code);
        });

        $id = $special->id;

        $category = \App\Models\SpecialCategory::Code($id, $cate)->first();

        $cate_id = $category->id;

        // 文章
        // 寬版(促銷)
        $wide = \Cache::remember($cate.'_wide', config('site.cache_expires'), function() use ($id, $cate_id)
        {
            return \App\Models\Article::Valid()->SpecialCate($id, $cate_id)->Wide()->orderByRaw('RAND()')->take(6)->get();
        });
        // 窄版(所有文章扣除寬版(促銷)六則)
        $narrow = \Cache::remember($cate.'_narrow', config('site.cache_expires'), function() use ($id, $cate_id, $wide)
        {
            return \App\Models\Article::Valid()->SpecialCate($id, $cate_id)->whereNotIn('id', $wide->lists('id'))->orderByRaw('RAND()')/*->take(15)*/->get();
        });
        $article = \Cache::remember($cate.'_article', config('site.cache_expires'), function() use ($id, $cate_id, $wide)
        {
            if ($wide->count() > 0) {
                return \App\Models\Article::Valid()->SpecialCate($id, $cate_id)->where('id', '<>', $wide[0]->id)->orderByRaw('RAND()')->take(5)->get();
            } else {
                return null;
            }
            
        });

		return view('pages.special')
            ->with('special', $special)
            ->with('wide', $wide)
            ->with('narrow', $narrow)
            ->with('article', $article);
	}

	/**
	 * 文章
	 *
	 * @return Response
	 */
	public function post($key, $id)
	{
        $special = \Cache::remember('special_'.$key, config('site.cache_expires'), function() use ($key)
        {
            return \App\Models\Special::Code($key);
        });

        // 文章
        $article = \Cache::remember('post_'.$id, config('site.cache_expires'), function() use ($id)
        {
            return \App\Models\Article::Valid()->find($id);
        });

        // 頻道分類
        $cates = $article->cate_mappings->lists('category_id');

        if ($article)
        {
            return view('pages.post')
                ->with('special', $special)
                ->with('article', $article)
                ->with('cates', $cates);
        }
        else
        {
            abort(404);
        }
	}

}
