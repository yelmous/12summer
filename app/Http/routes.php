<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Pattern */
Route::pattern('id', '\d+');
Route::pattern('slug', '[A-Za-z0-9-_]+');
Route::pattern('hash', '[a-z0-9]+');
Route::pattern('lang', '(en|zh|cn)');
Route::pattern('width', '\d+');
Route::pattern('height', '\d+');
Route::pattern('image_url', '[^\\s]+(\\.(?i)(jpg|png|gif|bmp))');

/* Resource */
/*Route::group(['middleware' => 'auth'], function() {
	Route::resource('article', 'Func\ArticleController');
	Route::resource('category', 'Func\CategoryController');
	Route::resource('slide', 'Func\SlideController');
	Route::resource('special', 'Func\SpecialController');
	Route::resource('user', 'Admin\UserController');
	Route::resource('zip', 'Func\ZipController');
});*/

/* Static */
Route::get('/{key}/post/{id}', array('uses' => 'HomeController@post'));
Route::get('/{key}/{slug}', array('uses' => 'HomeController@category'));
Route::get('/{key}', array('uses' => 'HomeController@special'));
Route::get('/', array('uses' => 'HomeController@index'));

/* Error */
Route::get('error', function()
{
	abort(503, 'Unauthorized action.');
});
