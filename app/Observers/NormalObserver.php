<?php namespace App\Observers;

class NormalObserver extends AbstractObserver {

	public function created($model)
	{
		$this->tableLog($model, config('message.create_log'));
	}

	public function updated($model)
	{
		$this->tableLog($model, config('message.update_log'));
	}

	public function deleting($model)
	{
		$this->tableLog($model, config('message.delete_log'));
	}

}
