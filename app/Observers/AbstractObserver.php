<?php namespace App\Observers;

use \Cache;

abstract class AbstractObserver {

//Model Events : creating, created, updating, updated, saving, saved, deleting, deleted, restoring, restored

//	protected function clearCacheTags($tags) {
//		Cache::tags($tags)->flush();
//	}
//
//	protected function clearCacheSections($section) {
//		Cache::section($section)->flush();
//	}

	protected function tableLog($model, $mode) {
		//Log to table
		$log = new \App\Models\TableLog;
		$log->table_name = $model->getTable();
		$log->table_key  = $model->getKey();
		$log->prev_info  = $model->toJson();
		$log->mode       = $mode;
		$log->user_id    = \Auth::user()->getAuthIdentifier();
		$log->ip         = getIP();
		$log->save();
	}

	abstract public function created($model);

	abstract public function updated($model);

	abstract public function deleting($model);

}
