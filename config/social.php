<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Google Analytics
	|--------------------------------------------------------------------------
	*/

	'ga_email'      => 'eyJpdiI6IkNMOXBnRkdENDR4Y2h5aGl1MWZVYTJxa3Jlb1Q4c1d3Ymh4VHlGQzA1ZTA9IiwidmFsdWUiOiJBbndodndma0J2YlpDcmhwZ0pubUtOajJPdmJRXC9LbHo1TndwV0FHRm1vQXhKKzdUY3ZTOWVXeDE2YUhmMmlYU3RPNTZNTWIxcENUbVVmZmpuakhocnc9PSIsIm1hYyI6ImU2YmZmZmJmZmFjZDg1ZjQxZjUwOTZiMDRhYTgwODM2OTQxYTU4ODA0MWMzMjJjNzg2ZThkN2NlNTlmNjM2Y2QifQ==',
    'ga_password'   => 'eyJpdiI6InQ0Z3M4RHhpa3MxMDRJMTlzM2JOUnlyNEc1cVBQWXBhSVp5cTJjeUZ6ODA9IiwidmFsdWUiOiJQMVUzckdmbm52eE1CNVJPZ2FZalwvOUlQank5MVhwTUIrSjZOVG14Y210bz0iLCJtYWMiOiI3YmYzNWFiNWYwNjlkMWI1MDMwZWE1YWRmNmRiZmZlNjdhOGE0NGVlYTY3NDMwNTIwYWUwMGJjMTMzODk0NGY0In0=',
    'ga_profile_id' => env('GA_PROFILE_ID'),
    'ga_key'        => env('GA_KEY'),
    'ga_custom'     => '',
    'is_ga'         => false,
    'is_ga_custom'  => false,

	/*
	|--------------------------------------------------------------------------
	| Facebook
	|--------------------------------------------------------------------------
	*/

    'fb_appid' => '561072340697306',
    'fb_admin' => '100000893655262',
    'facebook_fans_url' => 'https://www.facebook.com/design.wind.net',

	/*
	|--------------------------------------------------------------------------
	| Google+
	|--------------------------------------------------------------------------
	*/

    'google_plus_url' => 'http://plus.google.com/104630983415766741175',

	/*
	|--------------------------------------------------------------------------
	| Others
	|--------------------------------------------------------------------------
	*/

    'portfolio_url' => 'http://design-wind.me/',

);
