<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| SITE
	|--------------------------------------------------------------------------
	*/

    'site_name'        => 'Design-Wind.NET 設計風',
    'site_title'       => 'Design-Wind.NET 設計風 - 採用新技術及酷炫的特效，建立專屬風格的網站',
    'site_keyword'     => 'Design Wind, Magic Wind,設計風,設計,網站,虛擬空間,商業空間,平面設計,網路商店,專屬風格',
    'site_description' => '我們擅長設計美觀，並且符合國際規範的響應式網站 (Responsive Web Design，簡稱 RWD)。同時也會採用最新的技術，不管是 ASP.NET 或是 PHP，均可快速開發，以期讓客戶感受到最棒的網站操作經驗，採用新技術及酷炫的特效，建立專屬風格的網站。與我們一同瞭解、溝通您的需求，為您客製化具有設計風格的網站。',

    'copyright'        => 'Copyright © 2014-2015 Design-Wind.NET. All rights reserved.',
    'copyright_name'   => 'Design-Wind.NET 設計風',
    'copyright_desc'   => '【<b>Design-Wind.NET</b>】設計風',
    'copyright_link'   => 'http://design-wind.net/',

    'site_logo'        => 'assets/img/custom/smile_logo278x150.png',
    'site_home_logo'   => 'assets/img/custom/smile_logo90x50.png',
    'site_home'        => '微笑台灣',
    'site_home_url'    => 'http://smiletaiwan.cw.com.tw',

    // Mail
    'site_title_style' => 'background:#00a0da;width:100%;',
    'site_link_style'  => 'display:block;width:auto;height:auto;vertical-align:middle;padding-top:8px;',

	/*
	|--------------------------------------------------------------------------
	| SETTING
	|--------------------------------------------------------------------------
	*/

    'minify'        => env('MINIFY', false),
    'page_size'     => 15,

	/*
	|--------------------------------------------------------------------------
	| CDN
	|--------------------------------------------------------------------------
	*/

    'is_cdn'        => env('IS_CDN'),
    'cdn_url'       => '',

	/*
	|--------------------------------------------------------------------------
	| Cache
	|--------------------------------------------------------------------------
	*/

    'cache_flash'   => 'clear',
    'cache_expires' => 1,  // 分鐘

	/*
	|--------------------------------------------------------------------------
	| Timing
	|--------------------------------------------------------------------------
	*/

    'redirect_time' => 800,
    'notify_time'   => 8,

	/*
	|--------------------------------------------------------------------------
	| Others
	|--------------------------------------------------------------------------
	*/

	'devide' => '|',

	/*
	|--------------------------------------------------------------------------
	| Mail
	|--------------------------------------------------------------------------
	*/

    'service_mail'  => 'service@design-wind.net',
    'backup_mail'   => 'yelmous+319@gmail.com',

);
