<?php

\HTML::macro('thumb', function($img, $alt = null, $attributes = array(), $width = null, $height = null, $quality = 100, $crop = 1, $filter = null, $secure = false)
{
	$url = URL::to('assets/timthumb/timthumb.php?src='.$img.'&w='.$width.'&h='.$height.'&q='.$quality.'&zc='.$crop.'&f='.$filter);
	$url = ($secure == true) ? URL::to_secure($url) : URL::to($url);
	$img = \HTML::image($url, $alt, $attributes);

	return $img;
});
