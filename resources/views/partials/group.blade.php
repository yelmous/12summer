<div class="col-lg-6 col-md-6 col-sm-6">
    <div id="text-4" class="widget widget_text">
        <div class="textwidget">
            <h5><i class="icons icon-plus ic_dark"></i>{{ $setting->title }}</h5>
            <hr>
            <p>{{ $setting->description }}</p>
            <a class="button biggest variant2 wpb_btn-primary" data-animation="bounce" href="{{ $setting->value }}"><i class="icons icon-plus ic_single"></i>我要加入</a>
        </div>
    </div>
</div>
