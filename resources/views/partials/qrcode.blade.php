<div id="text-2" class="widget widget_text">
    <div class="textwidget">
        <h5><i class="icons icon-cloudapp ic_dark"></i>{{ $setting->title }}</h5>
        <hr>
        @if (empty($Setting->picture))
        <div id="qr_{{ $setting->param_id }}"></div>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('#qr_{{ $setting->param_id }}').kendoQRCode({
                    value: '{{ $setting->value }}',
                    errorCorrection: "Q",
                    size: 135,
                    color: "#00A0DA",
                    border: {
                        color: "#00A0DA",
                        width: 5
                    }
                });
            });
        </script>
        @else
        <img src="/assets/img/qrcode1.png" alt="" />
        @endif
        <p>{{ $setting->description }}</p>
    </div>
</div>
