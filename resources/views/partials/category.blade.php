<div class="row">
    <section class="col-lg-12 col-md-12 col-sm-12">
        <h3 class="special-text wow animated fadeInDown marine-header5-special-h3" data-animation="fadeInDown">{!! $special->overture !!}</h3>
        <h6 class="align-center marine-header4-h6" data-animation="">{!! $special->memo !!}</h6>
        <div class="row hidden-xs">
            @foreach($special->categories()->orderBy('order_by', 'asc')->get() as $cate)
            <div class="col-lg-3 col-md-3 col-sm-6">
                <a href="{{ URL::to( '/' . $special->special_code . '/' . $cate->cate_code ) }}">
                    <div class=" special-circle">
                        <img src="{{ URL::to( $cate->cate_image ) }}" class="aligncenter dont_scale wow animated fadeInDown" data-animation="fadeInDown" alt="">
                        <h3 class="special-text marine-header4-h3-style1" data-animation="">{{ $cate->cate_name }}</h3>
                        <div class="special-text marine-header4-special1" data-animation="">{{ $cate->cate_descript }}</div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <div class="clearfix clearfix-height30"></div>
    </section>
</div>
