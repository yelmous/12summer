@if (isset($articles[$idx]))
<div class="col-lg-8 col-md-8 col-sm-8">

    @include('partials.block', array('code' => $code, 'article' => $articles[$idx], 'type' => 'wide'))

</div>
@elseif ($alive)
<div class="col-lg-8 col-md-8 col-sm-8">

    @include('partials.blank2')

</div>
@endif
