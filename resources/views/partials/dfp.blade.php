<div class="col-lg-4 col-md-4 col-sm-4">
    <div class="shop-banner sp_bg" data-animation="">
        <img src="/assets/img/spacer.gif" class="rwd-md-h50 hidden-xs">
        <div class="banner-content-wrapper">
            <div class="banner-content">
                <div class="banner-content-inner sp_ad">

                   @include('partials.ad', array('key' => $key))

                </div>
            </div>
        </div>
    </div>
</div>
