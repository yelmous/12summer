<div class="shop-banner" data-animation="">
    @if($type == 'wide' && !empty($article->picture_wide))
    {!! HTML::thumb($article->picture_wide, $article->title, null, 755, 415) !!}
    @else
    {!! HTML::thumb($article->picture_narrow, $article->title, null, 360, 415) !!}
    @endif
    <div class="banner-content-wrapper">
        <div class="banner-content">
            <div class="banner-content-inner">
                <div class="overlay">
                    <a href="{{ URL::to('/'.$code.'/post/'.$article->id) }}">
                        <h3>{{ $article->title }}@if ($article->is_sponsor)<br/><span class="text-list">贊助</span>@endif</h3>
                    </a>
                    <h4>@if(empty($article->city_no))全台灣@else{{ $article->cityCode()->city }}@endif&nbsp;&nbsp;{{ $article->sub_title }}</h4>
                    {{--
                    <p>
                        @if ($type == 'wide')
                        <a href="{{ URL::to('/'.$code.'/post/'.$article->id) }}" class="button unfilled white">more</a>
                        @endif
                    </p>
                    --}}
                </div>
            </div>
        </div>
    </div>
</div>
