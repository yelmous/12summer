<div class="col-lg-4 col-md-4 col-sm-4">
    <div class="shop-banner sp_bg" data-animation="">
        @if (!empty($setting->picture))
        {!! HTML::thumb($setting->picture, $setting->title, null, 360, 415) !!}
        @else
        <img src="/assets/img/spacer.gif" class="rwd-md-h50"/>
        @endif
        <div class="banner-content-wrapper">
            <div class="banner-content">
                <div class="banner-content-inner">
                    <div class="overlay">
                        <h3>{{ $setting->description }}</h3>
                        {{--<h4>用行動深入鄉鎮區挖掘台灣的美麗！</h4>--}}
                        <!-- popup video -->
                        <div class="sc-videp-popup-wrapper center">
                            <a href="#" class="sc-open-video button unfilled white">微笑電影</a>
                            <div style="display: none;" class="sc-video-popup" data-url="{{ $setting->value }}" data-autoplay="yes">
                                <div class="sc-close-video">
                                    <i class="icons icon-cancel-circled"></i>
                                </div>
                            </div>
                        </div>
                        <!-- End popup video -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
