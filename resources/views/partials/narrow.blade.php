@if (isset($articles[$idx]))
<div class="col-lg-4 col-md-4 col-sm-4">

    @include('partials.block', array('code' => $code, 'article' => $articles[$idx], 'type' => 'narrow'))

</div>
@elseif ($alive)
<div class="col-lg-4 col-md-4 col-sm-4">

    @include('partials.blank')

</div>
@endif
