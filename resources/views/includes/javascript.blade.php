@if ( !config('site.minfy') )
<script type='text/javascript' src='/assets/js/bootstrap.min.js'></script>
<script type='text/javascript' src='/assets/js/jquery-ui.min.js'></script>
<script type='text/javascript' src='/assets/js/jquery.easing.1.3.js'></script>
<script type="text/javascript" src='/assets/js/jquery.migrate.js'></script>
<script type='text/javascript' src='/assets/js/jquery.mousewheel.min.js'></script>
<script type='text/javascript' src='/assets/js/SmoothScroll.min.js'></script>
<script type='text/javascript' src='/assets/js/backtop.js'></script>
<script type='text/javascript' src='/assets/js/prettyphoto/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='/assets/js/modernizr.js'></script>
<script type='text/javascript' src='/assets/js/wow.min.js'></script>
<script type='text/javascript' src='/assets/js/jquery.sharre.min.js'></script>
<script type='text/javascript' src='/assets/js/jquery.flexslider-min.js'></script>
<script type='text/javascript' src='/assets/js/jquery.knob.js'></script>
<script type='text/javascript' src='/assets/js/jquery.mixitup.min.js'></script>
<script type='text/javascript' src='/assets/js/masonry.min.js'></script>
<script type='text/javascript' src='/assets/js/jquery.masonry.min.js'></script>
<script type='text/javascript' src='/assets/js/jquery.fitvids.js'></script>
<script type='text/javascript' src='/assets/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js'></script>
<script type='text/javascript' src='/assets/js/jquery.nouislider.min.js'></script>
<script type='text/javascript' src='/assets/js/jquery.validity.min.js'></script>
<script type='text/javascript' src='/assets/js/tweetie.min.js'></script>
<script type='text/javascript' src='/assets/js/script.js'></script>
<script type='text/javascript' src='/assets/js/rs-plugin/js/jquery.themepunch.enablelog.js'></script>
<script type='text/javascript' src='/assets/js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
<script type='text/javascript' src='/assets/js/rs-plugin/js/jquery.themepunch.tools.min.js'></script>
@else
<script type='text/javascript' src='/assets/min/g=site.js'></script>
@endif
