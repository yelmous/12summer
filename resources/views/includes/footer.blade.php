<footer id="footer">
    <!-- Main Footer -->
    <div id="main-footer" class="smallest-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    @include('partials.qrcode', ['setting' => \App\Models\Setting::Set('SITE', 'pass319')])
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    @include('partials.qrcode', ['setting' => \App\Models\Setting::Set('SITE', 'mcard')])
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8">
                    @include('partials.group', ['setting' => \App\Models\Setting::Set('SITE', 'smiletaiwan')])
                    @include('partials.group', ['setting' => \App\Models\Setting::Set('SITE', 'sitetraveller')])
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="wysija-3" class="widget widget_wysija">
                            <h5><i class="icons icon-smile ic_dark"></i>關於微笑台灣</h5>
                            <hr>
                            <ul class="text-list">
                                <li>台北市104南京東路二段139號11樓</li>
                                <li>讀者服務專線：886-2-2662-0332</li>
                                <li>傳真電話：886-2-2662-6048</li>
                                <li>服務時間：週一～週五：9:00~17:30</li>
                            </ul>

                        </div>
                    </div>
                </div>

            </div>

            <hr>

            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-12">
                    <h6>主辦單位</h6>
                    <ul class="partner">
                        <li>
                            <a href="http://www.cw.com.tw/" title="天下雜誌" target="_blank"><img src="/upload/cw-logo.png" alt="" />
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <h6>指導單位</h6>
                    <ul class="partner">
                        <li>
                            <a href="http://taiwan.net.tw/" title="交通部觀光局" target="_blank"><img src="/upload/2015-07-06_182553.png" alt="" />
                            </a>
                        </li>
                        {{--<li>
                            <a href="#" target="_blank"><img src="/assets/img/footer/logo2.jpg" alt="" />
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><img src="/assets/img/footer/logo2.jpg" alt="" />
                            </a>
                        </li>--}}
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>
    <!-- /Main Footer -->
    <!-- Lower Footer -->
    <div id="lower-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <span class="copyright align-left">Copyright © 2015 天下雜誌. All rights reserved. 版權所有，禁止擅自轉貼節錄</span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <div id="social-media-2" class="widget widget_social_media align-right">
                        <ul class="social-media">
                            <li>
                                <div id="search-box">
                                    <div class="iconic-input">
                                        <input class="" name="" placeholder="全文搜索" type="text">
                                        <i class="icons icon-search"></i>
                                    </div>
                                </div>
                            </li>
                            <li class="tooltip-ontop" title="Facebook"><a href="javascript:void(0)" class="facebook"><i class="icon-facebook"></i></a></li>
                            <li class="tooltip-ontop" title="Twitter"><a href="javascript:void(0)" class="twitter"><i class="icon-twitter"></i></a></li>
                            {{--<li class="tooltip-ontop" title="Skype"><a href="#" class="skype"><i class="icon-skype"></i></a></li>--}}
                            <li class="tooltip-ontop" title="Google Plus"><a href="javascript:void(0)" class="googleplus"><i class="icon-google"></i></a></li>
                            {{--<li class="tooltip-ontop" title="Vimeo"><a href="#" class="vimeo"><i class="icon-vimeo"></i></a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Lower Footer -->
</footer>
