<section id="slider" class="hidden-xs">
    <div class="container">
        <!-- START REVOLUTION SLIDER 4.6.0 fullwidth mode -->
        <div id="rev_slider_11_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#eee;padding:0px;margin-top:0px;margin-bottom:0px;max-height:800px;">
            <div id="rev_slider_11_1" class="rev_slider fullwidthabanner" style="display:none;max-height:800px;height:800px;">
                <ul>
                    <!-- SLIDE  -->
                    @foreach($slides as $slide)
                    <li data-transition="fade" data-slotamount="" data-masterspeed="500" data-saveperformance="off" data-title="12個夏天">
                        <!-- MAIN IMAGE -->
                        <img src="/assets/img/rs-images/dummy.png" alt="" data-lazyload="{{ URL::to( $slide->slide_img ) }}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                        @if($slide->is_text)
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption big_extraheavy_170 skewfromrightshort tp-resizeme rs-parallaxlevel-0 customin customout" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:-100;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-x="left" data-hoffset="80" data-y="270" data-speed="400" data-start="1600" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">{{ $slide->title }}</div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption semibold_bigsub sfl tp-resizeme rs-parallaxlevel-0" data-x="left" data-hoffset="100" data-y="350" data-speed="400" data-start="2200" data-easing="Power0.easeIn" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">
                            <div style="" class="tp-layer-inner-rotation semibold_bigsub  rs-slideloop" data-easing="Power3.easeInOut" data-speed="2" data-xs="0" data-xe="0" data-ys="0" data-ye="0">
                                {{ $slide->content_1 }}<br>{{ $slide->content_2 }}
                            </div>
                        </div>
                        @endif
                        @if(!empty($slide->slide_url))
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption arrowicon tp-fade tp-resizeme rs-parallaxlevel-0" data-x="100" data-y="420" data-speed="400" data-start="2600" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">
                            <a href='{{ URL::to($slide->slide_url) }}' class='button biggest' style="color: #ffffff;background-color: transparent; border:2px solid #ffffff" @if($slide->is_new_window)target="_blank"@endif>更多內容</a>
                        </div>
                        @endif
                    </li>
                    @endforeach
                    <!-- SLIDE  -->
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
            <script type="text/javascript">
                /******************************************
                    -	PREPARE PLACEHOLDER FOR SLIDER	-
                ******************************************/
                @if ( config('app.debug') )
                var setREVStartSize = function () {
                    var tpopt = new Object();
                    tpopt.startwidth = 1170;
                    tpopt.startheight = 565;
                    tpopt.container = jQuery('#rev_slider_11_1');
                    tpopt.fullScreen = "off";
                    tpopt.forceFullWidth = "off";
                    tpopt.container.closest(".rev_slider_wrapper").css({
                        height: tpopt.container.height()
                    });
                    tpopt.width = parseInt(tpopt.container.width(), 0);
                    tpopt.height = parseInt(tpopt.container.height(), 0);
                    tpopt.bw = tpopt.width / tpopt.startwidth;
                    tpopt.bh = tpopt.height / tpopt.startheight;
                    if (tpopt.bh > tpopt.bw) tpopt.bh = tpopt.bw;
                    if (tpopt.bh < tpopt.bw) tpopt.bw = tpopt.bh;
                    if (tpopt.bw < tpopt.bh) tpopt.bh = tpopt.bw;
                    if (tpopt.bh > 1) {
                        tpopt.bw = 1;
                        tpopt.bh = 1
                    }
                    if (tpopt.bw > 1) {
                        tpopt.bw = 1;
                        tpopt.bh = 1
                    }
                    tpopt.height = Math.round(tpopt.startheight * (tpopt.width / tpopt.startwidth));
                    if (tpopt.height > tpopt.startheight && tpopt.autoHeight != "on") tpopt.height = tpopt.startheight;
                    if (tpopt.fullScreen == "on") {
                        tpopt.height = tpopt.bw * tpopt.startheight;
                        var cow = tpopt.container.parent().width();
                        var coh = jQuery(window).height();
                        if (tpopt.fullScreenOffsetContainer != undefined) {
                            try {
                                var offcontainers = tpopt.fullScreenOffsetContainer.split(",");
                                jQuery.each(offcontainers, function (e, t) {
                                    coh = coh - jQuery(t).outerHeight(true);
                                    if (coh < tpopt.minFullScreenHeight) coh = tpopt.minFullScreenHeight
                                })
                            } catch (e) {}
                        }
                        tpopt.container.parent().height(coh);
                        tpopt.container.height(coh);
                        tpopt.container.closest(".rev_slider_wrapper").height(coh);
                        tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);
                        tpopt.container.css({
                            height: "100%"
                        });
                        tpopt.height = coh;
                    } else {
                        tpopt.container.height(tpopt.height);
                        tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);
                        tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);
                    }
                };
                /* CALL PLACEHOLDER */
                setREVStartSize();
                var tpj = jQuery;
                tpj.noConflict();
                var revapi11;
                tpj(document).ready(function () {
                    if (tpj('#rev_slider_11_1').revolution == undefined)
                        revslider_showDoubleJqueryError('#rev_slider_11_1');
                    else
                        revapi11 = tpj('#rev_slider_11_1').show().revolution({
                            dottedOverlay: "none",
                            delay: 16000,
                            startwidth: 1170,
                            startheight: 565,
                            hideThumbs: 200,
                            thumbWidth: 100,
                            thumbHeight: 50,
                            thumbAmount: 0,
                            simplifyAll: "off",
                            navigationType: "bullet",
                            navigationArrows: "solo",
                            navigationStyle: "preview4",
                            touchenabled: "on",
                            onHoverStop: "on",
                            nextSlideOnWindowFocus: "off",
                            swipe_threshold: 0.7,
                            swipe_min_touches: 1,
                            drag_block_vertical: false,
                            parallax: "mouse",
                            parallaxBgFreeze: "on",
                            parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
                            keyboardNavigation: "off",
                            navigationHAlign: "center",
                            navigationVAlign: "bottom",
                            navigationHOffset: 0,
                            navigationVOffset: 20,
                            soloArrowLeftHalign: "left",
                            soloArrowLeftValign: "center",
                            soloArrowLeftHOffset: 20,
                            soloArrowLeftVOffset: 0,
                            soloArrowRightHalign: "right",
                            soloArrowRightValign: "center",
                            soloArrowRightHOffset: 20,
                            soloArrowRightVOffset: 0,
                            shadow: 0,
                            fullWidth: "on",
                            fullScreen: "off",
                            spinner: "spinner4",
                            stopLoop: "off",
                            stopAfterLoops: -1,
                            stopAtSlide: -1,
                            shuffle: "off",
                            autoHeight: "off",
                            forceFullWidth: "off",
                            hideThumbsOnMobile: "off",
                            hideNavDelayOnMobile: 1500,
                            hideBulletsOnMobile: "off",
                            hideArrowsOnMobile: "off",
                            hideThumbsUnderResolution: 0,
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            startWithSlide: 0
                        });
                }); /*ready*/
                @else
                var setREVStartSize=function(){var t=new Object;if(t.startwidth=1170,t.startheight=565,t.container=jQuery("#rev_slider_11_1"),t.fullScreen="off",t.forceFullWidth="off",t.container.closest(".rev_slider_wrapper").css({height:t.container.height()}),t.width=parseInt(t.container.width(),0),t.height=parseInt(t.container.height(),0),t.bw=t.width/t.startwidth,t.bh=t.height/t.startheight,t.bh>t.bw&&(t.bh=t.bw),t.bh<t.bw&&(t.bw=t.bh),t.bw<t.bh&&(t.bh=t.bw),t.bh>1&&(t.bw=1,t.bh=1),t.bw>1&&(t.bw=1,t.bh=1),t.height=Math.round(t.startheight*(t.width/t.startwidth)),t.height>t.startheight&&"on"!=t.autoHeight&&(t.height=t.startheight),"on"==t.fullScreen){t.height=t.bw*t.startheight;var e=(t.container.parent().width(),jQuery(window).height());if(void 0!=t.fullScreenOffsetContainer)try{var h=t.fullScreenOffsetContainer.split(",");jQuery.each(h,function(h,i){e-=jQuery(i).outerHeight(!0),e<t.minFullScreenHeight&&(e=t.minFullScreenHeight)})}catch(i){}t.container.parent().height(e),t.container.height(e),t.container.closest(".rev_slider_wrapper").height(e),t.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(e),t.container.css({height:"100%"}),t.height=e}else t.container.height(t.height),t.container.closest(".rev_slider_wrapper").height(t.height),t.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(t.height)};
                setREVStartSize();var tpj=jQuery;tpj.noConflict();var revapi11;tpj(document).ready(function(){void 0==tpj("#rev_slider_11_1").revolution?revslider_showDoubleJqueryError("#rev_slider_11_1"):revapi11=tpj("#rev_slider_11_1").show().revolution({dottedOverlay:"none",delay:16e3,startwidth:1170,startheight:565,hideThumbs:200,thumbWidth:100,thumbHeight:50,thumbAmount:0,simplifyAll:"off",navigationType:"bullet",navigationArrows:"solo",navigationStyle:"preview4",touchenabled:"on",onHoverStop:"on",nextSlideOnWindowFocus:"off",swipe_threshold:.7,swipe_min_touches:1,drag_block_vertical:!1,parallax:"mouse",parallaxBgFreeze:"on",parallaxLevels:[7,4,3,2,5,4,3,2,1,0],keyboardNavigation:"off",navigationHAlign:"center",navigationVAlign:"bottom",navigationHOffset:0,navigationVOffset:20,soloArrowLeftHalign:"left",soloArrowLeftValign:"center",soloArrowLeftHOffset:20,soloArrowLeftVOffset:0,soloArrowRightHalign:"right",soloArrowRightValign:"center",soloArrowRightHOffset:20,soloArrowRightVOffset:0,shadow:0,fullWidth:"on",fullScreen:"off",spinner:"spinner4",stopLoop:"off",stopAfterLoops:-1,stopAtSlide:-1,shuffle:"off",autoHeight:"off",forceFullWidth:"off",hideThumbsOnMobile:"off",hideNavDelayOnMobile:1500,hideBulletsOnMobile:"off",hideArrowsOnMobile:"off",hideThumbsUnderResolution:0,hideSliderAtLimit:0,hideCaptionAtLimit:0,hideAllCaptionAtLilmit:0,startWithSlide:0})});
                @endif
            </script>
        </div>
        <!-- END REVOLUTION SLIDER -->
        <script>
            /* Fix The Revolution Slider Loading Height issue */
            jQuery(document).ready(function ($) {
                $('.rev_slider_wrapper').each(function () {
                    $(this).css('height', '');
                    var revStartHeight = parseInt($('>.rev_slider', this).css('height'));
                    $(this).height(revStartHeight);
                    $(this).parents('#slider').height(revStartHeight);

                    $(window).load(function () {
                        $('#slider').css('height', '');
                    });
                });
            });
        </script>
    </div>
</section>
