<div id="main-header">
    <div class="container">
        <div class="row">
            <!-- Logo -->
            <div class="col-lg-2 col-md-2 col-sm-2 logo">
                <a href="{{ URL::to('/') }}" title="微笑台灣319" rel="home"><img class="logo" src="/assets/img/logo_smiletaiwan.png" alt="微笑台灣319">
                </a>
                <div id="main-nav-button">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 align-right">
                <div class="item right hidden-separator hidden-sm hidden-xs">
                    <ul id="menu-sub-header" class="menu">
                        <li class="menu-item"><a href="http://www.319.com.tw/custompage/show/44">關於微笑台灣</a>
                        </li>
                        <li class="menu-item"><a href="http://www.319.com.tw/epaper/newpaper.html">微笑電子報</a>
                        </li>
                        <li class="menu-item"><a href="http://www.319.com.tw/">客服中心</a>
                        </li>
                        <li class="menu-item"><a href="http://www.319.com.tw/loginPage">登入</a>
                        </li>
                        <li class="menu-item">
                            <!-- Search Box -->
                            <div id="search-box">
                                <div class="iconic-input">
                                    <input class="" name="" placeholder="全文搜索" type="text">
                                    <i class="icons icon-search"></i>
                                </div>
                            </div>
                            <!-- /Search Box -->
                        </li>
                    </ul>
                    {{--
                    <ul class="social-media">
                        <li><a href="javascript:void(0)" class="facebook"><i class="icon-facebook"></i></a></li>
                        <li><a href="javascript:void(0)" class="twitter"><i class="icon-twitter"></i></a></li>
                        <li><a href="javascript:void(0)" class="googleplus"><i class="icon-google"></i></a></li>
                    </ul>
                    --}}
                </div>


                <!-- Main Navigation -->
                <ul id="main-nav" class="menu">
                    <li class="menu-item ">
                        <a href="{{ URL::to('/').'/'.$special->special_code }}" class="active">特別企劃：{{ $special->special_name }}</a>
                    </li>
                    <li class="menu-item"><a href="http://www.319.com.tw/village/">鄉鎮素描</a></li>
                    <li class="menu-item"><a href="http://www.319.com.tw/store/all">微笑聯盟</a></li>
                    <li class="menu-item"><a href="http://www.319.com.tw/custompage/show/42">雲端護照</a></li>
                    <li class="menu-item"><a href="http://www.319.com.tw/mcard/">旅行明信片</a></li>
                    <li class="menu-item"><a href="http://www.319.com.tw/">旅人專欄</a></li>
                    <li class="menu-item"><a href="http://www.319.com.tw/">最新消息</a></li>
                    {{--這一段在Pc版隱藏--}}
                    @if(isMobile())
                    <li class="menu-item hidden-lg"><a href="http://www.319.com.tw/custompage/show/44">關於微笑台灣</a></li>
                    <li class="menu-item hidden-lg"><a href="http://www.319.com.tw/epaper/newpaper.html">微笑電子報</a></li>
                    <li class="menu-item hidden-lg"><a href="http://www.319.com.tw/">客服中心</a></li>
                    <li class="menu-item hidden-lg"><a href="http://www.319.com.tw/loginPage">登入</a></li>
                    <li class="menu-item hidden-lg">
                        <!-- Search Box -->
                        <div id="search-box">
                            <div class="iconic-input">
                                <input class="" name="" placeholder="全文搜索" type="text">
                                <i class="icons icon-search"></i>
                            </div>
                        </div>
                        <!-- /Search Box -->
                    </li>
                    @endif
                    {{--這一段在Pc版隱藏--}}
                </ul>
                <!-- /Main Navigation -->
            </div>
        </div>
    </div>
</div>
