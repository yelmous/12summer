<!--319GA code start-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63644196-1', 'auto');
  ga('send', 'pageview');

</script>
<!--319GA code end-->

<!--天下GA code start-->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-1198057-9', 'auto');
    ga('send', 'pageview');

</script>
<!--天下GA code end-->

<!--天下alexa code start-->
<script type="text/javascript">
    _atrk_opts = {atrk_acct: "MUvYh1aon800iC", domain: "cw.com.tw", dynamic: true};
    (function () {
        var as = document.createElement('script');
        as.type = 'text/javascript';
        as.async = true;
        as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js";
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(as, s);
    })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=MUvYh1aon800iC" style="display:none" height="1" width="1" alt="" /></noscript>
<!--天下alexa code end-->

<!--天下Comscode code start-->
<script>
    var _comscore = _comscore || [];
    _comscore.push({c1: "2", c2: "12333885"});
    (function () {
        var s = document.createElement("script"),
            el = document.getElementsByTagName("script")[0];
        s.async = true;
        s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
        el.parentNode.insertBefore(s, el);
    })();
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=12333885&cv=2.0&cj=1" />
</noscript>
<!--天下Comscode code end-->
