@if ( !config('site.minfy') )
<link rel='stylesheet' id='kendo-css' href='/assets/css/kendo.common.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='twitter-bootstrap-css' href='/assets/css/bootstrap.min.css' type='text/css' media='all'/>

<link rel='stylesheet' id='piglobal-css' href='/assets/css/piglobal.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='typo-css' href='/assets/css/typo.min.css' type='text/css' media='all'/>

<link rel='stylesheet' id='fontello-css' href='/assets/css/fontello.css' type='text/css' media='all'/>
<link rel='stylesheet' id='prettyphoto-css-css' href='/assets/js/prettyphoto/css/prettyPhoto.css' type='text/css' media='all'/>
<link rel='stylesheet' id='animation-css' href='/assets/css/animation.css' type='text/css' media='all'/>
<link rel='stylesheet' id='perfectscrollbar-css' href='/assets/css/perfect-scrollbar-0.4.10.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-validity-css' href='/assets/css/jquery.validity.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-ui-css' href='/assets/css/jquery-ui.min.css' type='text/css' media='all'/>
<link rel='stylesheet' id='style-css' href='/assets/css/style.css' type='text/css' media='all'/>
<link rel='stylesheet' id='mobilenav-css' href='/assets/css/mobilenav.css' type='text/css' media="screen and (max-width: 989px)"/>
<link rel="stylesheet" type="text/css" href="/assets/css/style.revslider.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="/assets/js/rs-plugin/css/settings.css" media="screen"/>
@else
<link rel='stylesheet' href='/assets/min/g=site.css' type='text/css' media='all'/>
<link rel='stylesheet' id='mobilenav-css' href='/assets/css/mobilenav.min.css' type='text/css' media="screen and (max-width: 989px)"/>
<link rel='stylesheet' href='/assets/min/g=screen.css' type='text/css' media='screen'/>
@endif
