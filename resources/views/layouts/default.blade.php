<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="isie ie8 oldie no-js"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="isie ie9 no-js"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    @section('meta')
    <meta name="description" content="{{ setting('SITE', 'description') }}"/>
    <meta name="keywords" content="{{ setting('SITE', 'keywords') }}"/>
    @show
    {{-- Open Graph --}}
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:url" content="@yield('url')" />
    @section('open_graph')
    <meta property="og:image" content="{{ setting('SITE', 'image') }}" >
    <meta property="og:description" content="{{ setting('SITE', 'description') }}" />
    @show
    <meta property="og:site_name" content="{{ config('site.site_title') }}"/>
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="zh_TW" />
    <meta property="fb:app_id" content="{{ config('social.fb_appid') }}" />
    {{--<meta property="article:author" content="https://www.facebook.com/smiletaiwan319" />--}}
    <meta property="article:publisher" content="https://www.facebook.com/smiletaiwan319" />
    <!-- Title -->
    <title>@yield('title') - {{ config('site.site_home') }}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="/assets/img/319icon.ico" type="image/x-icon"/>
    <!-- Google Fonts -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700,800' rel='stylesheet' type='text/css'/>
    <link href='//fonts.googleapis.com/css?family=Fira+Sans:300italic' rel='stylesheet' type='text/css'/>
    <!-- Stylesheets -->
    @section('style')
        @include('includes.style')
    @show
    <!-- jQuery -->
    <script src="/assets/js/jquery-1.11.1.min.js"></script>
    <script src="/assets/js/kendo.all.min.js"></script>
    <!--[if lt IE 9]>
        <script>
            document.createElement("header");
            document.createElement("nav");
            document.createElement("section");
            document.createElement("article");
            document.createElement("aside");
            document.createElement("footer");
            document.createElement("hgroup");
        </script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="/assets/js/html5.js"></script>
    <![endif]-->
    <!--[if lt IE 7]>
        <script src="/assets/js/icomoon.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <link href="/assets/css/ie.css" rel="stylesheet">
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="/assets/js/jquery.placeholder.js"></script>
        <script src="/assets/js/script_ie.js"></script>
    <![endif]-->

    {{-- AD script --}}
    @yield('ad_head_script')
    {{-- /AD script --}}

</head>

<body class="w1170 headerstyle8 @yield('body_css')">

    @include('includes.ga')

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.3&appId={{ config('social.fb_appid') }}";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Content Wrapper -->
    <div id="marine-content-wrapper">
        <header id="header" class="style8">

            {{-- AD - LOGO 728x90 --}}
            @yield('ad_logo_u_728x90')
            {{-- /AD - LOGO 728x90 --}}

            <!-- Main Header -->
            @include('includes.header', ['special' => $special])
            <!-- /Main Header -->
        </header>

        <!-- Content Inner -->
        <div id="marine-content-inner">
            <!-- Main Content -->
            <section id="main-content" @yield('main_content_css')>

                <!-- Slide -->
                @include('includes.slide', ['slides' => $special->slides()])
                <!-- /Slide -->

                @yield('breadcrumbs')

                <div class="container">

                    @yield('content')

                </div>
                <!-- /Container -->
            </section>
            <!-- /Main Content -->
        </div>
        <!-- /Conten Inner -->

        <!-- Footer -->
        @include('includes.footer')
        <!-- /Footer -->

    </div>
    <!-- /Content Wrapper -->
    <a href="#0" class="cd-top">Top</a>
    <!-- JavaScript -->
    @include('includes.javascript')
    @if ( !config('site.minify') )
    <script type="text/javascript" defer>
        jQuery(document).ready(function () {
            jQuery('.tp-banner').show().revolution({
                dottedOverlay: "none",
                delay: 16000,
                startwidth: 1170,
                startheight: 700,
                hideThumbs: 200,
                thumbWidth: 100,
                thumbHeight: 50,
                thumbAmount: 5,
                navigationType: "bullet",
                navigationArrows: "solo",
                navigationStyle: "preview2",
                touchenabled: "on",
                onHoverStop: "on",
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
                parallax: "mouse",
                parallaxBgFreeze: "on",
                parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
                keyboardNavigation: "off",
                navigationHAlign: "center",
                navigationVAlign: "bottom",
                navigationHOffset: 0,
                navigationVOffset: 20,
                soloArrowLeftHalign: "left",
                soloArrowLeftValign: "center",
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,
                soloArrowRightHalign: "right",
                soloArrowRightValign: "center",
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,
                shadow: 0,
                fullWidth: "on",
                fullScreen: "off",
                spinner: "spinner4",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                forceFullWidth: "off",
                hideThumbsOnMobile: "off",
                hideNavDelayOnMobile: 1500,
                hideBulletsOnMobile: "off",
                hideArrowsOnMobile: "off",
                hideThumbsUnderResolution: 0,
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                videoJsPath: "rs-plugin/videojs/",
                fullScreenOffsetContainer: ""
            });
        }); //ready
    </script>
    @else
    <script type="text/javascript" defer>
        jQuery(document).ready(function(){jQuery(".tp-banner").show().revolution({dottedOverlay:"none",delay:16e3,startwidth:1170,startheight:700,hideThumbs:200,thumbWidth:100,thumbHeight:50,thumbAmount:5,navigationType:"bullet",navigationArrows:"solo",navigationStyle:"preview2",touchenabled:"on",onHoverStop:"on",swipe_velocity:.7,swipe_min_touches:1,swipe_max_touches:1,drag_block_vertical:!1,parallax:"mouse",parallaxBgFreeze:"on",parallaxLevels:[7,4,3,2,5,4,3,2,1,0],keyboardNavigation:"off",navigationHAlign:"center",navigationVAlign:"bottom",navigationHOffset:0,navigationVOffset:20,soloArrowLeftHalign:"left",soloArrowLeftValign:"center",soloArrowLeftHOffset:20,soloArrowLeftVOffset:0,soloArrowRightHalign:"right",soloArrowRightValign:"center",soloArrowRightHOffset:20,soloArrowRightVOffset:0,shadow:0,fullWidth:"on",fullScreen:"off",spinner:"spinner4",stopLoop:"off",stopAfterLoops:-1,stopAtSlide:-1,shuffle:"off",autoHeight:"off",forceFullWidth:"off",hideThumbsOnMobile:"off",hideNavDelayOnMobile:1500,hideBulletsOnMobile:"off",hideArrowsOnMobile:"off",hideThumbsUnderResolution:0,hideSliderAtLimit:0,hideCaptionAtLimit:0,hideAllCaptionAtLilmit:0,startWithSlide:0,videoJsPath:"rs-plugin/videojs/",fullScreenOffsetContainer:""})});
    </script>
    @endif
    <script type="text/javascript" defer>
        jQuery('.facebook').click(function() {
            FB.ui({
              method: 'share',
              href: '@yield('url')',
            }, function(response){});
        });
        jQuery('.twitter').click(function() {
            window.open('http://twitter.com/home/?status='.concat(encodeURIComponent(document.title)).concat(' ').concat(encodeURIComponent(location.href)));
        });
        jQuery('.googleplus').click(function() {
            window.open('https://plus.google.com/share?url='.concat(encodeURIComponent(location.href)), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
        });
    </script>
    @yield('custom_js')
</body>

</html>
