<div class="post blog-post blog-post-classic col-lg-12 col-md-12 col-sm-12 margin-top20">
    <div class="blog-post-list">
        <div class="blog-post-content">

            <div class="page-heading style3 wrapper border-bottom ">

                <div class="blog-post-meta">
                    <span class="post-format">
                        <span class="sub1-icon"></span>
                    </span>
                    <ul class="post-meta">
                        <li><a href="{{ URL::to('/'.$special->special_code) }}">{{ $special->special_name }}</a></li>
                        <li><a href="#">旅人專欄</a></li>
                        {{--<li><a href="#">南台灣</a></li>--}}
                        <li>{{ date('Y-m-d', strtotime($article->created_at)) }}</li>
                    </ul>
                </div>

                <div class="post-title">
                    <h1>{{ $article->title }} @if($article->is_sponsor)<span class="text-list">贊助</span>@endif</h1>
                    <h5>{{ $article->sub_title }}</h5>
                </div>

                {{--<hr>--}}

                <div class="post-about">
                    <dl class="dl-horizontal">
                        @if(!empty($article->author))
                        <dt>作者</dt>
                        <dd>{{ $article->author }}</dd>
                        @endif
                        @if(!empty($article->photography))
                        <dt>攝影</dt>
                        <dd>{{ $article->photography }}</dd>
                        @endif
                        <dt>關聯鄉鎮</dt>
                        <dd>
                        @if(empty($article->city_no))
                        全台灣
                        @else
                        {{ $article->cityCode()->city }} / @include('article.area', ['article' => $article]) {{-- $article->zipCode->area --}}
                        @endif
                        </dd>
                    </dl>
                </div>

                {{--<hr>--}}

            </div>

            <!-- Post Content -->
            <div class="post-content">

                <!-- Post Image -->
                <div class="post-thumbnail">
                    {!! HTML::image($article->picture_narrow, $article->title) !!}
                    <div class="post-hover">
                        <a class="link-icon" href="{{ URL::to('/'.$special->special_code.'/post/'.$article->id) }}"></a>
                        <a class="search-icon prettyPhoto" href="{{ URL::to($article->picture_narrow) }}"></a>
                    </div>
                </div>
                <!-- /Post Image -->

                {!! $article->content !!}

            </div>
            <!-- /Post Content -->
        </div>
    </div>
</div>
