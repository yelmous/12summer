@foreach($similars as $similar)
<div class="col-lg-12 col-md-12 col-sm-12">
    <div style="visibility: visible; animation-name: fadeInDown;" class="morestory sc-icon wow animated fadeInDown animated">
        <img class="icons" src="/assets/img/blue-arr.png" alt="">
        <h3 class="marine-seo-vectors-h3"><a href="{{ URL::to('/'.$code.'/post/'.$similar->id) }}">{{ $similar->title }}</a></h3>
        <p class="marine-seo-p-color">
            {{ $similar->preview }}
        </p>
    </div>
</div>
@endforeach
