@if($article->boxes->count() > 0)
<div class="blogleftrecom">
    <div class="recomtitle"></div>
    <div id="owl-example" class="owl-carousel">

        @foreach($article->boxes as $box)
        <div class="item">
            <div class="pic">
                <div class="pic-arr"></div>
                {!! HTML::thumb($box->picture, $box->title, null, 355, 195) !!}
            </div>
            <div class="picinfo">
                <h3><a href="#">{{ $box->title }}</a></h3>
                {!! $box->content !!}
            </div>
        </div>
        @endforeach

    </div>
</div>
@endif
