@extends('layouts.default')

@section('title', $article->title)

@section('url'){{ URL::to('/'.$special->special_code.'/post/'.$article->id) }}@stop

@section('meta')
    @if(!empty($article->meta_description))
    <meta name="description" content="{{ str_replace( '\n', '', $article->meta_description ) }}"/>
    @else
    <meta name="description" content="{{ str_replace( '\n', '', setting('SITE', 'description') ) }}"/>
    @endif
    @if(!empty($article->meta_description))
    <meta name="keywords" content="{{ str_replace( '\n', '', $article->meta_keyword ) }}"/>
    @else
    <meta name="keywords" content="{{ str_replace( '\n', '', setting('SITE', 'keywords') ) }}"/>
    @endif
@stop

@section('open_graph')
    <meta property="og:image" content="@if($article->is_promote){{ URL::to($article->picture_wide) }}@else{{ URL::to($article->picture_narrow) }}@endif">
    <meta property="og:description" content="{{ $article->preview }}" />
@stop

@section('style')
    @parent
    <!-- Owl Carousel Assets -->
    <link href="/assets/js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="/assets/js/owl-carousel/owl.theme.css" rel="stylesheet">
@stop

{{-- AD script --}}
@section('ad_head_script')

    <script type='text/javascript'>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
      (function() {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') +
          '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
      })();
    </script>

    <script type='text/javascript'>
      googletag.cmd.push(function() {
        googletag.defineSlot('/47573522/topic_content_r_u_300x600', [300, 600], 'div-gpt-ad-1432872264131-0').addService(googletag.pubads());
        googletag.defineSlot('/47573522/topic_content_r_d_300x250', [300, 250], 'div-gpt-ad-1432872264131-1').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>

@stop

@section('main_content_css', 'class="gray_bg"')

@section('breadcrumbs')

    <div class="breadcrumbs">
        <div class="container">
            <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="{{ config('site.site_title') }}" href="{{ URL::to('/') }}" class="home">首頁</a></span>
            <span property="v:title">{{ $special->special_name }}</span>
            <span property="v:title">{{ $article->title }}</span>
        </div>
    </div>

@stop

@section('content')

<div class="row">
    <section class="main-content col-lg-8 col-md-8 col-sm-8 small-padding">
        <div class="blogleft">
            <div class="row">
                <div id="post-items">

                    <!-- Post Item -->
                    @include('article.article', ['article' => $article])
                    <!-- /Post Item -->
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-12">
                    @include('article.share', ['special' => $special, 'article' => $article])
                </div>
            </div>
        </div>

        <div class="clearfix-height30"></div>
        @include('article.box', ['article' => $article])
        <div class="row">
            @include('article.similar', ['code' => $special->special_code, 'similars' => $article->similars($special->id)])
            <div class="col-lg-12 col-md-12 col-sm-12 align-right">
                <a class="button biggest variant2 wpb_btn-inverse" data-animation="bounce" href="{{ URL::to('/'.$special->special_code) }}"><i class="icons icon-angle-right ic_single"></i>更多夏天故事</a>
            </div>
        </div>

    </section>
    <aside class="sidebar col-lg-4 col-md-4 col-sm-4">

        <div id="categories-3" class="widget widget_categories">
            <ul>
                <li class="cat-item">
                    <a href="{{ URL::to('/'.$special->special_code) }}"><i class="icon-left-open-3 ic_dark"></i>回上頁</a>
                </li>
                @foreach($special->categories as $cate)
                <li class="cat-item @if(in_array($cate->id, (array)$cates)) active @endif">
                    <a href="{{ URL::to( '/' . $special->special_code . '/' . $cate->cate_code ) }}"><i class="icon-left-open-3 ic_dark"></i>{{ $cate->cate_name }}</a>
                </li>
                @endforeach
            </ul>
        </div>

        <div class="widget text-center">
            <!-- /47573522/topic_content_r_u_300x600 -->
            @include('partials.ad', array('key' => 'topic_content_r_u_300x600'))
        </div>


        <div class="blogright widget">
            <div class="fb-page" data-href="https://www.facebook.com/smiletaiwan319" data-width="100%" data-height="600" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/smiletaiwan319"><a href="https://www.facebook.com/smiletaiwan319">微笑台灣319鄉+</a></blockquote></div>               </div>
            <div class="clearfix"></div>
        </div>


        <div class="blogright-b widget">
            <img src="/assets/img/title_family_blue.png" alt="" />
            <div class="family"></div>
            <div class="clearfix"></div>
        </div>

        <div class="widget text-center">
            <!-- /47573522/topic_content_r_d_300x250 -->
            @include('partials.ad', array('key' => 'topic_content_r_d_300x250'))
        </div>

        {{-- 暫時移除
        <div class="blogright widget">
            <!-- textcloud  -->
            <script type="text/javascript">
                jQuery(document).ready(function ($) {

                    var tagclude_a = $("#tagclude").find("a");
                    tagclude_a.each(function () {
                        var x = 9;
                        var y = 0;
                        var rand = parseInt(Math.random() * (x - y + 1) + y);
                        $(this).addClass("size" + rand);
                    });

                });
            </script>
            <!-- size1 to 9  -->
            <div class="taglist" id="tagclude">
                <a class="size1" href="#">微笑台灣</a>
                <a class="size2" href="#">美食天堂</a>
                <a class="size3" href="#">好家在台灣</a>
                <a class="size4" href="#">319鄉鎮</a>
                <a class="size5" href="#">微笑台灣</a>
                <a class="size6" href="#">美食天堂</a>
                <a class="size7" href="#">好家在台灣</a>
                <a class="size8" href="#">319鄉鎮</a>
                <a class="size9" href="#">12個夏天</a>
            </div>
            <!-- End textcloud  -->
        </div>
        --}}

    </aside>
</div>

<!-- 微笑聯盟  -->
{{--
@include('article.smileteam')
--}}
<!-- /微笑聯盟  -->

@stop

@section('custom_js')

    <!-- owl -->
    <script src='/assets/js/owl-carousel/owl.carousel.js'></script>
    @if ( !config('site.minify') )
        <script type="text/javascript" defer>
            jQuery(document).ready(function ($) {
                $("#owl-example").owlCarousel();
            });
            jQuery("#owl-example").owlCarousel({
                // Most important owl features
                items : 1,
                itemsCustom : false,
                itemsDesktop : [1199,1],
                itemsDesktopSmall : [980,1],
                itemsTablet: [768,1],
                itemsTabletSmall: false,
                itemsMobile : [479,1],
                singleItem : false,
                itemsScaleUp : false,

                //Basic Speeds
                slideSpeed : 200,
                paginationSpeed : 800,
                rewindSpeed : 1000,

                //Autoplay
                autoPlay : 10000,
                stopOnHover : true,

                // Navigation
                navigation : false,
                navigationText : ["prev","next"],
                rewindNav : true,
                scrollPerPage : false,

                //Pagination
                pagination : true,
                paginationNumbers: false,
            });
        </script>
    @else
        <script type="text/javascript" defer>
            jQuery("#owl-example").owlCarousel({items:1,itemsCustom:!1,itemsDesktop:[1199,1],itemsDesktopSmall:[980,1],itemsTablet:[768,1],itemsTabletSmall:!1,itemsMobile:[479,1],singleItem:!1,itemsScaleUp:!1,slideSpeed:200,paginationSpeed:800,rewindSpeed:1e3,autoPlay:1e4,stopOnHover:!0,navigation:!1,navigationText:["prev","next"],rewindNav:!0,scrollPerPage:!1,pagination:!0,paginationNumbers:!1}),jQuery(document).ready(function(e){e("#owl-example").owlCarousel()});
        </script>
    @endif

@stop
