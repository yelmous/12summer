@extends('layouts.default')

@section('title', $special->special_name)

@section('url'){{ URL::to('/') . '/' . $special->special_code }}@stop

@section('body_css', 'preheader-on')

{{-- AD script --}}
@section('ad_head_script')

    <script type='text/javascript'>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
      (function() {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') +
          '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
      })();
    </script>

    <script type='text/javascript'>
      googletag.cmd.push(function() {
        googletag.defineSlot('/47573522/topic_list_360x340', [360, 340], 'div-gpt-ad-1432872158661-0').addService(googletag.pubads());
        googletag.defineSlot('/47573522/topic_list_300x250_1', [300, 250], 'div-gpt-ad-1432872158661-1').addService(googletag.pubads());
        googletag.defineSlot('/47573522/topic_list_300x250_2', [300, 250], 'div-gpt-ad-1432872158661-2').addService(googletag.pubads());
        googletag.defineSlot('/47573522/topic_home_logo_u_728x90', [728, 90], 'div-gpt-ad-1432872158661-3').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>

@stop

{{-- AD - LOGO 728x90 --}}
@section('ad_logo_u_728x90')

    <div id="upper-header">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 align-center">
                    <div class="item hidden-separator">

                        <!-- /47573522/topic_home_logo_u_728x90 -->
                        @include('partials.ad', array('key' => 'topic_home_logo_u_728x90'))

                    </div>
                </div>


            </div>
        </div>
    </div>

@stop

@section('content')

<div class="page-description">
    <div class="full_bg full-width-bg marine-header7-page-bg" data-animation="">

        <!-- 4icon -->
        @include('partials.category', array('special' => $special))

        <!-- 1-12 -->
        <div class="row">
            @include('partials.wide', array('code' => $special->special_code, 'articles' => $wide, 'idx' => 0, 'alive' => true))
            
            {{-- 此區塊只有行動裝置顯示 --}}
            @if(isMobile() && !empty($article) && $article->count() > 0)
            <div class="col-sm-4 visible-xs">
                <div class="pi-section-white">
                    <h4 class="pi-has-bg pi-weight-700 pi-uppercase pi-letter-spacing pi-margin-bottom-20">特別推薦</h4>
                    <ul class=" pi-list-with-icons pi-list-icons-right-dir pi-list-dashed">
                        @foreach($article as $item)
                        <li><a href="{{ URL::to('/'.$special->special_code.'/post/'.$item->id) }}">{{ $item->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
            {{-- 此區塊只有行動裝置顯示 --}}

            @if ( empty(setting('SITE', 'video')) )
                <!-- /47573522/topic_list_360x340 -->
                @include('partials.dfp', array('key' => 'topic_list_360x340'))
            @else
                @include('partials.video', array('setting' => App\Models\Setting::Set('SITE', 'video')))
            @endif
        </div>

        <div class="row">
            <!-- /47573522/topic_list_300x250_1 -->
            @include('partials.dfp', array('key' => 'topic_list_300x250_1'))
            @include('partials.narrow', array('code' => $special->special_code, 'articles' => $narrow, 'idx' => 0, 'alive' => true))
            @include('partials.narrow', array('code' => $special->special_code, 'articles' => $narrow, 'idx' => 1, 'alive' => true))
        </div>

        <div class="row">
            @include('partials.narrow', array('code' => $special->special_code, 'articles' => $narrow, 'idx' => 2, 'alive' => true))
            @include('partials.wide', array('code' => $special->special_code, 'articles' => $wide, 'idx' => 1, 'alive' => true))
        </div>

        <div class="row">
            @include('partials.wide', array('code' => $special->special_code, 'articles' => $wide, 'idx' => 2, 'alive' => true))
            <!-- /47573522/topic_list_300x250_2 -->
            @include('partials.dfp', array('key' => 'topic_list_300x250_2'))
        </div>

        <div class="row">
            @include('partials.narrow', array('code' => $special->special_code, 'articles' => $narrow, 'idx' => 3, 'alive' => true))
            @include('partials.narrow', array('code' => $special->special_code, 'articles' => $narrow, 'idx' => 4, 'alive' => true))
            @include('partials.narrow', array('code' => $special->special_code, 'articles' => $narrow, 'idx' => 5, 'alive' => true))
        </div>

        <!-- banner -->
        <div class="full_bg full-width-bg marine-header4-creative-full_bg" data-animation="">
            <h2 class="special-text marine-header4-h2" data-animation="">
                {!! $special->overture2 !!}
            </h2>
            <h4 class="special-text wow animated fadeInLeft marine-header4-h4" data-animation="fadeInLeft">{!! $special->overture !!}</h4>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix clearfix-height30"></div>
        <!-- /banner -->

        <!-- others -->
        <div class="row">
            <?php
                $n = 6;
            ?>
            @while ($n < $narrow->count())
                @include('partials.narrow', array('code' => $special->special_code, 'articles' => $narrow, 'idx' => $n++, 'alive' => false))
            @endwhile
        </div>
        <!-- /others -->

        <div class="clearfix"></div>
    </div>
</div>

@stop
