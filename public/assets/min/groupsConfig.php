<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/**
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(

    'site.css' => array('//assets/css/kendo.common.min.css', '//assets/css/bootstrap.min.css', '//assets/css/piglobal.min.css', '//assets/css/typo.min.css', '//assets/css/fontello.css', '//assets/js/prettyphoto/css/prettyPhoto.css', '//assets/css/animation.css', '//assets/css/perfect-scrollbar-0.4.10.min.css', '//assets/css/jquery.validity.css', '//assets/css/jquery-ui.min.css', '//assets/css/style.css'),

    'screen.css' => array('//assets/css/style.revslider.css', '//assets/js/rs-plugin/css/settings.css'),

    'site.js' => array('//assets/js/bootstrap.min.js', '//assets/js/jquery-ui.min.js', '//assets/js/jquery.easing.1.3.js', '//assets/js/jquery.migrate.js', '//assets/js/jquery.mousewheel.min.js', '//assets/js/SmoothScroll.min.js', '//assets/js/backtop.js', '//assets/js/prettyphoto/js/jquery.prettyPhoto.js', '//assets/js/modernizr.js', '//assets/js/wow.min.js', '//assets/js/jquery.sharre.min.js', '//assets/js/jquery.flexslider-min.js', '//assets/js/jquery.knob.js', '//assets/js/jquery.mixitup.min.js', '//assets/js/masonry.min.js', '//assets/js/jquery.masonry.min.js', '//assets/js/jquery.fitvids.js', '//assets/js/perfect-scrollbar-0.4.10.with-mousewheel.min.js', '//assets/js/jquery.nouislider.min.js', '//assets/js/jquery.validity.min.js', '//assets/js/tweetie.min.js', '//assets/js/script.js', '//assets/js/rs-plugin/js/jquery.themepunch.enablelog.js', '//assets/js/rs-plugin/js/jquery.themepunch.revolution.min.js', '//assets/js/rs-plugin/js/jquery.themepunch.tools.min.js'),

);
